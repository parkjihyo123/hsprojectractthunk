import React from 'react';
import { PersistGate } from 'redux-persist/integration/react'
import { Provider } from 'react-redux'
import persist from './src/config/store'
import Main from './src/main';
import NavtigationS from './src/page/navigation/index';
const persistStore = persist();


export default function App() {
  return (
    
    <Provider store={persistStore.store}>
      <PersistGate loading={null} persistor={persistStore.persistor}>
      <NavtigationS/>
      </PersistGate>   
    </Provider>
  );
}
