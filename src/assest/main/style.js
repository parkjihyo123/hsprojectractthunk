import {StyleSheet} from 'react-native';

const styleLogin = StyleSheet.create(
    {
       logintext:{
           height:40,
           borderColor:'gray',
           borderWidth:1
       },
       contrainer:{
           backgroundColor:'#718792',
           flex:1,
           alignItems:'center',
           justifyContent:'center'
       }
    }
)
export default styleLogin