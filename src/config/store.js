import thunk from 'redux-thunk';
import AsyncStorage from '@react-native-community/async-storage';
import { combineReducers, createStore, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import loginReducer from "../reducers/auth.reducer";
const AppReduces = combineReducers({
    loginReducer,
});
const rootReducer =(state,action)=>{
    return AppReduces(state,action);
}
const persistConfig = {
    key: 'root',
    storage: AsyncStorage
  }
const persistedReducer = persistReducer(persistConfig, rootReducer)
export default () => {
  let store = createStore(persistedReducer,{},applyMiddleware(thunk))
  let persistor = persistStore(store)
  return { store, persistor }
}