import React from 'react';
import { StyleSheet, Text, View, TextInput,Dimensions,TouchableOpacity } from 'react-native';
import Logo from '../../components/Login/LoginLogoComponent'
import Icon from  'react-native-vector-icons/Ionicons'
const {width:WIDTH}= Dimensions.get('window');
export default function Forgotpassword()
{
    return (
        <View style={styles.container}>
            <Logo/>           
               <View>
               <Icon name="ios-person" size={28} color="rgb(7, 7, 7)" style={styles.inputIcon}/>
                   <TextInput style={styles.inputBox} 
                placeholder="Email" placeholderTextColor="white"
              />
              </View>             
              <TouchableOpacity style={styles.button}>
                  <Text style={styles.buttonText}>Gửi</Text>
              </TouchableOpacity>
            </View>
    );
}
const styles = StyleSheet.create({
    container:{
        backgroundColor:'#718792',
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
    inputBox:{
        width:WIDTH -55,
        height:45,
        backgroundColor:'rgba(255,255,255,0.3)',
        borderRadius:25,
        paddingLeft:40,
        fontSize:20,
        paddingHorizontal:10,
        marginVertical:10,
        color:'white',       
    },button:{
        width:300,
        backgroundColor:"#1c313a",
        borderRadius:25,
        marginVertical:10,
        paddingVertical:10
    },buttonText:{
        fontSize :18,
        fontWeight:'500',
        color:'white',
        textAlign:"center"
    },
    inputIcon:{
        position:'absolute',
        top:16,
        left:12
    }
})