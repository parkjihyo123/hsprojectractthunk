import React from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';
import Logo from '../../components/Login/LoginLogoComponent'
import FromComponent from '../../components/Login/Fromcomponent';
import Forgotpassword from './ForgotPasswordPage';
import { Actions } from 'react-native-router-flux';
export default class LoginScreen extends React.Component
{
    constructor(props)
    {
        super(props);
    }
    render(){
        return (
            <View style={styles.container}>
                <Logo/>
                <FromComponent/>     
                <View style={styles.forgot}>
                    <Text style={styles.forgotext} onPress={()=>this.props.navigation.push('ForgotPassword')}>Quên Mật Khẩu?</Text>
                    </View>      
            </View>
        );
    }   
}
const styles = StyleSheet.create({
    container:{
        backgroundColor:'#718792',
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
    forgot:{
        flexGrow:1,
        alignItems:"center",
        justifyContent:"flex-end",
        marginVertical:16
    },forgotext:{
        fontSize:16,
        color:'white'
    }
})