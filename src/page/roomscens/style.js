import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
    container:{
        backgroundColor:'white',
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:10
    },inputIcon:{
        position:'absolute',
        top:16,
        left:12,
        color:'red'
    }
});