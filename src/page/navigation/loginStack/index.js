import React from  'react';
import {createStackNavigator} from "@react-navigation/stack";
import LoginScreen from '../../Login/LoginPage';
import Forgotpassword from '../../Login/ForgotPasswordPage';
const LoginStack = createStackNavigator();

export default function LoginStackScreen() {
    return(
    <LoginStack.Navigator headerMode={"none"}>
    <LoginStack.Screen name="Login" component={LoginScreen}/>
    <LoginStack.Screen name="ForgotPassword" component={Forgotpassword}/>
    </LoginStack.Navigator>
    )
}