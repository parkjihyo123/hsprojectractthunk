import React from 'react';
import {StyleSheet,Image} from 'react-native';
import { useWindowDimensions } from 'react-native';
import Icon from  'react-native-vector-icons/Ionicons';
import {Platform}  from 'react-native';
import { createDrawerNavigator,DrawerContentScrollView,DrawerItemList,DrawerItem} from '@react-navigation/drawer';
import { Text, View } from 'native-base';
import LoginStackScreen from '../loginStack/index';
import HomePageStackScreen from '../mainStack/index';
import {RoomNumberScreen} from '../../roomscens/roomnumberscene';
const Drawer = createDrawerNavigator();
function Test(){
  return(
    <View style={{alignContent:'center',alignItems:'center',justifyContent:'center'}}>
    <Image source={require('../../../assest/main/img/AdminLTELogo.png')}/>
    <Text style={{bottom:20}} >Xin Chao !!</Text>
    </View>
  );
}

function CustomDrawerContent(props) {
  return (
    <DrawerContentScrollView {...props}>    
      <Test/>         
      <DrawerItemList {...props} />   
    </DrawerContentScrollView>
  );
}
export default function NavigationStack()
{
  const dimensions = useWindowDimensions();
  
  return(
    
    <Drawer.Navigator drawerType={dimensions.width >=768 ?'permanent':'front'}  drawerContent={props=><CustomDrawerContent{...props}/>}>
      <Drawer.Screen options={{drawerIcon:config=><Icon size={22}name={Platform.OS === 'android' ? 'md-list' : 'ios-list'}></Icon>}}  name="HomePage" component={HomePageStackScreen} />        
      <Drawer.Screen name="Section2" component={RoomNumberScreen}/>  
    </Drawer.Navigator>
  )
}

const styles = StyleSheet.create({
  container:{   
    backgroundColor:'#fff',
    alignItems:'center',
    justifyContent:'center'
  }
})
