import React from 'react';
import {createStackNavigator } from '@react-navigation/stack';
import DashboardScreens from '../../dashboard_screens/index';
import {RoomNumberScreen} from '../../roomscens/roomnumberscene';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Icon from  'react-native-vector-icons/Ionicons';
import {styles} from './style';
const HomePageStack = createStackNavigator();
export default function HomePageStackScreen ({ navigation }){    
        return(
            <HomePageStack.Navigator initialRouteName="DashBoard">
                <HomePageStack.Screen  name="DashBoard" component={DashboardScreens} options={{headerLeft:()=>(<Icon size={32} style={styles.IconLeftHeader} name="ios-menu" onPress={()=>navigation.toggleDrawer()}/>),}}/>          
                <HomePageStack.Screen  name="Rooms" component={RoomNumberScreen}/>                                              
            </HomePageStack.Navigator>
        )   
}