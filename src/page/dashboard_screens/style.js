import {StyleSheet} from 'react-native';
export const styles = StyleSheet.create({
    container:{
        backgroundColor:'white',
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    }
});