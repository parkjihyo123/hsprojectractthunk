const initialState ={
        userData:{},
        isFetching:false,
        isError:false
    };
const loginReducer =(state=initialState,action)=>
{
switch(action.type){
    case 'LOGINING':
        return Object.assign({},state,{
               userData:{},
               isFetching:true,
               isLogined:false,
               isError :false 
        });
    case 'LOGINED':
            return Object.assign({},state,{
                userData:action.data,
                isFetching :false,
                isLogined:true,
                isError:false
            });
    case 'LOGINERROR':
            return Object.assign({},state,{
                    userData:{},
                    isFetching:false,
                    isLogined:false,
                    isError: true
            })            
}
return state;
}
export default loginReducer;