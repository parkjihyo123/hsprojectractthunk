import React,{Component} from 'react';
import {StyleSheet,Text,View,StatusBar,TouchableOpacity} from 'react-native'
import Routes from '../src/page/RoutePage/RoutePage'
import { connect } from 'react-redux';

class Main extends Component{
    render()
    {
        return(
            <View style={styles.container}>
                <Routes/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1
    }
});
export default connect(null,null)(Main)