import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput,TouchableOpacity,Dimensions} from 'react-native';
import Icon from  'react-native-vector-icons/Ionicons'
const {width:WIDTH}= Dimensions.get('window');
import {callApiLogin} from '../../actions/LoginAction';
import { connect } from "react-redux";
class FromComponent extends Component
{
    constructor(props){
        super(props);
        this.state={
          username:'',
          password:''
        }
    }
    testfuntion =()=>{
    this.props.dispatch(callApiLogin(this.state.username,this.state.password));
    }
    render()
    {
        return (
            <View style={styles.container}>
               <View>
               <Icon name="ios-person" size={28} color="rgb(7, 7, 7)" style={styles.inputIcon}/>
                   <TextInput style={styles.inputBox} 
                placeholder="Email" placeholderTextColor="white" onChangeText={(text)=>this.setState({username:text})}
              /></View>
              <View>
              <Icon name="ios-key" size={28} color="rgb(7, 7, 7)" style={styles.inputIcon}/>
              <TextInput style={styles.inputBox} placeholder="Password" placeholderTextColor="white" secureTextEntry={true} onChangeText={(text)=>this.setState({password:text})}/> 
            </View>   
              <TouchableOpacity style={styles.button} onPress={this.testfuntion.bind(this)}>
                  <Text style={styles.buttonText}>Đăng Nhập</Text>
              </TouchableOpacity>
            </View>
        );
    }
    
}
const styles = StyleSheet.create({
    container:{      
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
    inputBox:{
        width:WIDTH -55,
        height:45,
        backgroundColor:'rgba(255,255,255,0.3)',
        borderRadius:25,
        paddingLeft:45,
        fontSize:20,
        paddingHorizontal:16,
        marginVertical:10,
        color:'white'
    },button:{
        width:300,
        backgroundColor:"#1c313a",
        borderRadius:25,
        marginVertical:10,
        paddingVertical:12
    },buttonText:{
        fontSize :18,
        fontWeight:'500',
        color:'white',
        textAlign:"center"
    },
    inputIcon:{
        position:'absolute',
        top:16,
        left:12
    }
})
const mapStateToProps = state => {
    return {
      data: state
    };
  };
  export default connect(mapStateToProps)(FromComponent);