import React ,{Component} from 'react'
import {Router,Stack,Scene} from 'react-native-router-flux'
import Forgotpassword from '../ForgotPasswordComponent'
import Login from '../Logincomponent'
export default class Routes extends Component{
    render()
    {
        return(
            <Router>
                <Stack key="root" hideNavBar={true}>
                    <Scene key="login" component={Login} title="Login" initial={true}/>
                    <Scene key="Forgotpassword" component={Forgotpassword} title="ForgotPassword"/>
                </Stack>
            </Router>
        )
    }
}