import persist from '../config/store'
import {RequestApiAsyncPost} from '../api_config/api';
const persistStore = persist();
export const fetch_logining=()=>{
    return {
        type:"LOGINING"
    };
};
export const receive_Login = post => {
    return {
      type: "LOGINED",
      data: post
    };
  };
  
  export const receive_error = () => {
    return {
      type: "LOGINERROR"
    };
  };

  export  const callApiLogin = (username,password)=>{
        persistStore.store.dispatch(fetch_logining());    
        var temp ={
          Username:username,
          Password:password
        }      
        return function(dispatch,getstate){
          console.log(password);
          RequestApiAsyncPost('account/login()','POST',{},temp,null).then(respone=>{
            console.log(respone.data.data);
            if (respone.data.satus === false) {
                throw new Error("No such user found!!");
              } else dispatch(receive_Login(respone.data.data));
        }).catch(function (error) {
          dispatch(receive_error())
        });     
          // axios({
          //   method: 'post',
          //   url: 'http://bcdhapi.com/api/account/login()',
          //   data: JSON.stringify(temp),
          //   headers: {'Content-Type':'application/json'}
          // })                      
        }
  }